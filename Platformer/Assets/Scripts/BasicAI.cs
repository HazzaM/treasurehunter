﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicAI : MonoBehaviour
{

    public float speed = 0.5f;
    public Transform target;

    public float attackRange;
    public int damage;
    private float lastAttackTime;
    public float attackDelay;

    private Rigidbody2D myRigidbody;

    // Use this for initialization
    void Start()
    {
        
    }

        // Update is called once per frame
        void Update()
    {

        //Movement
            Vector3 displacement = target.position - transform.position;
        displacement = displacement.normalized;

        if (Vector2.Distance(target.position, transform.position) > 1.0f)
        {
            transform.position += (displacement * speed * Time.deltaTime);

        }

        //Distance between the ghost and player
        float distanceToPlayer = Vector3.Distance (transform.position, target.position);
        if (distanceToPlayer < attackRange) {

            //Has enough time passed since last attack
            if (Time.time > lastAttackTime + attackDelay)
                target.SendMessage("TakeDamage", damage);
        }

            //Record last attack time
            lastAttackTime = Time.time;

        }
}

       

    
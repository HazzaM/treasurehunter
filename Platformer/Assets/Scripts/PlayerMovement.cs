﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : Health
    
{
    private Rigidbody2D myRigidbody;

    //Movement, Grounding and AirControl
    [SerializeField]
    private float movementSpeed;

    private bool Attack;

    public Transform attackPos;
    public float attackRange;
    public LayerMask whatIsEnemies;

    private bool facingRight;

    [SerializeField]
    private Transform[] groundPoints;

    [SerializeField]
    private float groundRadius;

    [SerializeField]
    private LayerMask whatIsGround;

    private bool isGrounded;

    private bool PlayerJump;

    [SerializeField]
    private bool airControl;

    [SerializeField]
    private float jumpForce;

    private Animator animator;

    bool isDead;

    //END OF VARIABLES//

    

    void Start()
    {
        facingRight = true;
        myRigidbody = GetComponent<Rigidbody2D>();

        animator = GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");

        isGrounded = IsGrounded();

        HandleMovement(horizontal);

        Flip(horizontal);

        HandleAttacks();

        ResetValues();
    }

    private void HandleMovement(float horizontal)
    {
        if (isDead == true)
            return;

        if (!this.animator.GetCurrentAnimatorStateInfo(0).IsTag("Attack")&& (isGrounded || airControl))
        {

            myRigidbody.velocity = new Vector2(horizontal * movementSpeed, myRigidbody.velocity.y);

        }
        else if (this.animator.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
        {

            myRigidbody.velocity = Vector2.zero;
        }

        if (isGrounded && PlayerJump)
        {
            isGrounded = false;
            myRigidbody.AddForce(new Vector2(0, jumpForce));
        }

    }

    //Player Death
    private void Death()
    {
        

        //Respawn will be implemented in Menu UI (To-Do List)
    }

    //Attack Animation Trigger
    private void HandleAttacks()
    {
        if (isDead == true)
            return;

        if (Attack && (!this.animator.GetCurrentAnimatorStateInfo(0).IsTag("Attack")))
        {
            animator.SetTrigger("Attack");
            myRigidbody.velocity = Vector2.zero;
        }
    }

    private void HandleInput()
    {
        if (isDead == true)
            return;

        if (Input.GetKeyDown(KeyCode.W))
        {
            PlayerJump = true;
        }


        if (Input.GetKeyDown(KeyCode.Space))
        {
            Attack = true;
        }
    }

    private void Flip(float horizontal)
    {
        if (isDead == true)
            return;

        if (horizontal > 0 && !facingRight || horizontal <0 && facingRight)
        {
            facingRight = !facingRight;

            Vector3 theScale = transform.localScale;

            theScale.x *= -1;

            transform.localScale = theScale;
        }
    }

    
    void Update()
    {
        MoveCharacter();
        HandleInput();

        if (Input.GetKey(KeyCode.Space))
        {
            Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemies);
            
        }

        if (isDead == true)
        {
            if (Input.GetKeyDown(KeyCode.R))
                Application.LoadLevel(Application.loadedLevel);

            return;
        }
    }

    //(Blackthornprod, 2018, HOW TO MAKE 2D MELEE COMBAT - EASY UNITY TUTORIAL, 00:04:28)
    //Used to help with player attack/gizmos
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);
    }

    void MoveCharacter()
    {
        if (isDead == true)
            return;

        if (Input.GetKey(KeyCode.W))
        {
            animator.SetBool("PlayerJump", true);
        }
        else
        {
            animator.SetBool("PlayerJump", false);
        }
    
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A))
        {
            animator.SetBool("PlayerRun", true);
        }
        else
        {
            animator.SetBool("PlayerRun", false);
        }

        if (Input.GetKey(KeyCode.S))
        {
            animator.SetBool("PlayerDash", true);
        }
        else
        {
            animator.SetBool("PlayerDash", false);
        }

        if (isPlayerDead == true)
        {
            animator.SetBool("PlayerDeath", true);
            isDead = true;
        }
        else
        {
            animator.SetBool("PlayerDeath", false);
        }
        
      

    }

    private void ResetValues()
    {
        Attack = false;
        PlayerJump = false;
    }

    //(inScope Studios, 2015, Unity 5 tutorial for beginners: 2D Platformer - Jumping, 00:04:35)
    //Used to help the player find ground + physics
    private bool IsGrounded()
    {
        if (myRigidbody.velocity.y <= 0)
        {
            foreach (Transform point in groundPoints)
            {
                Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position, groundRadius, whatIsGround);
                for (int i = 0; i < colliders.Length; i++)
                {
                    if (colliders[i].gameObject != gameObject)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}